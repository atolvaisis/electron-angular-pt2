import { Component, OnInit, Input, OnChanges, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { FileService } from '../../services/file.service';
import { FileInfo } from '../../models/file-info';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit, OnChanges {

  @Input()
  currentPath: string;

  supportedExtensions: string[] = ['.png', '.jpg', '.jpeg', '.gif', '.bmp', '.svg'];

  imageFiles: FileInfo[];

  constructor(private fileService: FileService, private changeDetector: ChangeDetectorRef) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.currentPath.currentValue == null || changes.currentPath.currentValue === '') {
      this.imageFiles = [];
    } else {
      const currentDirectory = changes.currentPath.currentValue;
      this.fileService.getFiles(currentDirectory, this.supportedExtensions).subscribe(f => {
        this.imageFiles = f;
        this.changeDetector.detectChanges();
      });
    }
  }

}
