import { TestBed, inject } from '@angular/core/testing';

import { RequiredModulesService } from './required-modules.service';

describe('RequiredModulesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RequiredModulesService]
    });
  });

  it('should be created', inject([RequiredModulesService], (service: RequiredModulesService) => {
    expect(service).toBeTruthy();
  }));
});
