import { Injectable } from '@angular/core';

function _window(): any {
  // return the global browser window object
  return window;
}

@Injectable()
export class RequiredModulesService {

  constructor() { }

  get browserWindow(): any {
    return _window();
  }

  get fs(): any {
    return this.browserWindow.NODE_FS;
  }

  get os(): any {
    return this.browserWindow.NODE_OS;
  }

  get path(): any {
    return this.browserWindow.NODE_PATH;
  }

}
