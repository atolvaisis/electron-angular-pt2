import { Injectable } from '@angular/core';
import { RequiredModulesService } from './required-modules.service';
import { Observable } from 'rxjs/Observable';
import { FileInfo } from '../models/file-info';

@Injectable()
export class FileService {

  constructor(private requiredModulesService: RequiredModulesService) { }

  private getDirectoryContents(directoryPath: string): Observable<FileInfo> {
    const readDirAsObservable = Observable.bindNodeCallback(
      (path: string, callback: (error: Error, files: string[]) => void) => this.requiredModulesService.fs.readdir(path, callback)
    );

    const lstatAsObservable = Observable.bindNodeCallback(
      (path: string, callback: (error: Error, stats: any) => void) => this.requiredModulesService.fs.lstat(path, callback)
    );

    return readDirAsObservable(directoryPath)
      .flatMap(f => f)
      .mergeMap(f => lstatAsObservable(this.requiredModulesService.path.join(directoryPath, f)).catch(err => Observable.empty())
        .map(s => {
          const fileInfo = new FileInfo();
          fileInfo.fullPath = this.requiredModulesService.path.join(directoryPath, f);
          fileInfo.name = f;
          fileInfo.isDirectory = s.isDirectory();
          fileInfo.url = new URL(`file:///${fileInfo.fullPath}`).href;
          return fileInfo;
      }));
    }

  getDirectories(directoryPath: string): Observable<FileInfo[]> {
    return this.getDirectoryContents(directoryPath).filter(f => f.isDirectory).toArray();
  }

  getFiles(directoryPath: string, extensions: string[] = []): Observable<FileInfo[]> {
    return this.getDirectoryContents(directoryPath).filter(f => {
      const extension = this.requiredModulesService.path.extname(f.name);
        return extensions.length === 0 ?
          f.isDirectory === false :
          f.isDirectory === false && (extensions.find(e => e === extension) != null);
    }).toArray();
  }

  getUpDirectory(directoryPath: string): FileInfo {
    const upDirectory = this.requiredModulesService.path.join(directoryPath, '..');
    const upDirInfo = new FileInfo();
    upDirInfo.name = '..';
    upDirInfo.fullPath = upDirectory;
    upDirInfo.isDirectory = true;
    return upDirInfo;
  }

  getHomeDirectoryPath(): string {
    return this.requiredModulesService.os.homedir();
  }

}
