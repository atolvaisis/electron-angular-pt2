import { Component, ChangeDetectorRef } from '@angular/core';
import { FileInfo } from './models/file-info';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  currentPathInfo: FileInfo;

  constructor(private changeDetector: ChangeDetectorRef) {}

  currentPathChanged(fileInfo: FileInfo) {
    this.currentPathInfo = fileInfo;
    this.changeDetector.detectChanges();
  }

}
